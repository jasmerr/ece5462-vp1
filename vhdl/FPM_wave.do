onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -radix hexadecimal /fpmtb/A
add wave -noupdate -radix hexadecimal /fpmtb/B
add wave -noupdate -radix hexadecimal /fpmtb/C
add wave -noupdate -radix hexadecimal /fpmtb/exp_res
add wave -noupdate /fpmtb/latch
add wave -noupdate /fpmtb/drive
add wave -noupdate -radix ascii /fpmtb/aid_sig
add wave -noupdate -radix ascii /fpmtb/bid_sig
add wave -noupdate -radix ascii /fpmtb/resid_sig
add wave -noupdate /fpmtb/err_sig
add wave -noupdate -radix decimal /fpmtb/score
add wave -noupdate /fpmtb/nm
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {0 ns} 0}
quietly wave cursor active 0
configure wave -namecolwidth 150
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {32950 ns} {33950 ns}

