library ieee;
use ieee.std_logic_1164.all;
entity fpa is
  PORT (A,B : IN std_logic_vector (31 downto 0);
        latch, drive: IN std_ulogic;
        C : OUT std_logic_vector (31 downto 0));
end fpa;

architecture data_flow of fpa is

signal Aint,Bint,Cint : std_logic_vector(31 downto 0);
signal signa,signb,signr : bit;
signal expa,expb,lgrexp : bit_vector(7 downto 0);
signal nexpa,nexpb,bma,amb : bit_vector(7 downto 0);
signal bmac,ambc : bit_vector(8 downto 0);
signal fract_a,fract_b : bit_vector(22 downto 0);
signal sd1 : bit_vector(7 downto 0);
signal sdc : bit;
signal shift_dist : bit_vector(4 downto 0);
signal expgt,expeq,explt : bit;
signal expa0,expa1,expb0,expb1 : bit;
signal mangt,maneq,manlt,mana0,manb0 : bit;
signal adenorm,bdenorm,swap : bit;
signal lxbarin,rxbarin,xbar_r,xbar_l : bit_vector(23 downto 0);
signal mux_l,mux_r : bit_vector(23 downto 0);
signal mux_r_ctl : bit;
signal shft1,shft2,shft3,shft4,shft5 : bit_vector(23 downto 0);
signal shft_d : std_logic_vector (7 downto 0);
signal l_adder_in,r_adder_in : bit_vector(24 downto 0);
signal addres,adder_c : bit_vector(24 downto 0);
signal dec2,dec1,fract0 : bit;
signal ldg1fract,fp_ctl,l1pos : bit_vector(22 downto 0);
signal lding1pos,ls_dist : bit_vector(4 downto 0);
signal rpl,l1res : bit_vector(22 downto 0);
signal expvalp1,p1carry : bit_vector(7 downto 0);
signal expp1all1 : bit;
signal adjexp,exp_val_adj : bit_vector(8 downto 0);
signal exp_sel_p1,exp_sel_lgr,underflow,adjexp0,lgrall0 : bit;
signal exp_sel_adj,exp_sel_0 : bit;
signal exp_val_p1: bit_vector(7 downto 0);
signal resshf,adjexpc : bit_vector(8 downto 0);
signal exp_norm : bit_vector(7 downto 0);
signal man_sel_ls,man_sel_ls_rs,man_sel_val,man_sel_0 : bit;

signal man_lshf1,man_lshf2,man_lshf3,man_lshf4,man_lshf5 : 
                                             bit_vector(24 downto 0);
signal man_ls_rs,man_rs1,man_norm : bit_vector(24 downto 0);


constant zeroexp : bit_vector(7 downto 0) := "00000000";
constant oneexp  : bit_vector(7 downto 0) := "11111111";
constant zeroman : bit_vector(22 downto 0) := "00000000000000000000000";
constant smanzero : bit_vector(23 downto 0) := "000000000000000000000000";
constant nan_man : bit_vector(23 downto 0) := "000000000000000000000001";
constant zero_23 : bit_vector(22 downto 0) := "00000000000000000000000";
constant zero_24 : bit_vector(24 downto 0) := "0000000000000000000000000";
constant HighZ : std_logic_vector(31 downto 0) 
                           :="ZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZZ";

BEGIN

  latch_in : PROCESS
  BEGIN
    wait until rising_edge(latch);
    Aint <= A;  Bint <= B;
  END PROCESS latch_in;

  -- breakout signals for computation
  signa <= To_bit(Aint(31));
  signb <= To_bit(Bint(31));
  expa <= To_bitvector(Aint(30 downto 23));
  expb <= To_bitvector(Bint(30 downto 23));
  fract_a <= To_bitvector(Aint(22 downto 0));
  fract_b <= To_bitvector(Bint(22 downto 0));
  -- generate exponent signals
  expgt <= '1' when (expa > expb) else '0';
  expeq <= '1' when (expa = expb) else '0';
  explt <= '1' when (expa < expb) else '0';
  expa0 <= '1' when (expa = zeroexp) else '0';
  expa1 <= '1' when (expa = oneexp) else '0';
  expb0 <= '1' when (expb = zeroexp) else '0';
  expb1 <= '1' when (expb = oneexp) else '0';
  -- and difference in exponents and the larger
  lgrexp <= expb when (explt = '1') else expa;
  nexpa <= NOT expa;   nexpb <= NOT expb;
  bmac(0) <= '1';
  bma <= expb XOR nexpa XOR bmac(7 downto 0);
  bmac(8 downto 1) <= (expb AND nexpa) OR (expb AND bmac(7 downto 0))
                        OR (nexpa AND bmac(7 downto 0));
  ambc(0) <= '1';
  amb <= expa XOR nexpb XOR ambc(7 downto 0);
  ambc(8 downto 1) <= (expa AND nexpb) OR (expa AND ambc(7 downto 0))
                        OR (nexpb AND ambc(7 downto 0));
  -- select the correct shift distance
  sd1 <= bma when (explt = '1') else amb;
  sdc <= not sd1(7) and not sd1(6) and not sd1(5);
  shift_dist <= sd1(4 downto 0) when (sdc = '1') else "11111";
  -- mantissa signal generation
  mangt <= '1' when (fract_a > fract_b) else '0';
  maneq <= '1' when (fract_a = fract_b) else '0';
  manlt <= '1' when (fract_a < fract_b) else '0';
  mana0 <= '1' when (fract_a = zeroman) else '0';
  manb0 <= '1' when (fract_b = zeroman) else '0';
  -- sign of the result
  signr <= signa when ((expgt OR (expeq AND mangt)) = '1') else
           signb when ((explt OR (expeq AND manlt)) = '1') else
           signa AND signb;
  -- expand the mantissas and get correct form
  adenorm <= expa0 and NOT mana0;
  bdenorm <= expb0 and NOT manb0;
  lxbarin <= fract_a & '0' when (adenorm = '1') else ((NOT expa0) & fract_a);
  rxbarin <= fract_b & '0' when (bdenorm = '1') else ((NOT expb0) & fract_b);
  -- see if need to swap using x-bar
  swap <= expgt OR (expeq AND mangt);
  xbar_r <= lxbarin when (swap = '1') else rxbarin;
  xbar_l <= rxbarin when (swap = '1') else lxbarin;
  -- larger is on right output of xbar
  -- on left mux zero if either exponent all 1's
  mux_l <= smanzero when ((expa1 OR expb1) = '1') else xbar_l;
  mux_r_ctl <= expa1 AND mana0 AND expb1 AND manb0 AND (signa xor signb);
  mux_r <= nan_man when (mux_r_ctl = '1') else xbar_r;
  -- right linear shift left argument by shift dist 
  shft1 <= '0' & mux_l(23 downto 1) when(shift_dist(0) = '1') else mux_l;
  shft2 <= "00" & shft1(23 downto 2) when(shift_dist(1) = '1') else shft1;
  shft3 <= "0000" & shft2(23 downto 4) when(shift_dist(2) = '1') else shft2;
  shft4 <= "00000000" & shft3(23 downto 8) when(shift_dist(3) = '1')
                                               else shft3;
  shft5 <= "0000000000000000" & shft4(23 downto 16) when(shift_dist(4) = '1')
                                               else shft4;
  l_adder_in <= '1' & NOT shft5 when ((signa XOR signb) = '1')
                else '0' & shft5;
  r_adder_in <= '0' & mux_r;
  --
  -- add the two arguments  THE CORE ADDER
  --
  adder_c(0) <= signa XOR signb;
  addres <= l_adder_in XOR r_adder_in XOR adder_c;
  adder_c(24 downto 1) <= (l_adder_in(23 downto 0) AND r_adder_in(23 downto 0))
       OR  (l_adder_in(23 downto 0) AND adder_c(23 downto 0))
       OR  (r_adder_in(23 downto 0) AND adder_c(23 downto 0));
  --
  -- now need to normalize the results
  -- step 1 is to modify the fractional part to be 0--01xxx---x
  ldg1fract <= addres (22 downto 0);
  fp_ctl(22) <= '0';
  fp_ctl(21 downto 0) <= fp_ctl(22 downto 1) OR ldg1fract(22 downto 1);
  l1pos <= ldg1fract AND (NOT fp_ctl);
  --  first generate output signals from the add result
  dec2 <= addres(24);
  dec1 <= addres(23);
  fract0 <= '1' when (addres(22 downto 0) = zero_23) else '0';
  -- zero all but leading 1 in fract part
--          rpl <= rpl or addres(22 downto 0);
--          l1res <= (not rpl) AND addres(22 downto 0);
  l1res <= l1pos;
  lding1pos(0) <= l1res(22) OR l1res(20) OR l1res(18) OR l1res(16) OR
       l1res(14) OR l1res(12) OR l1res(10) OR l1res(8) OR
       l1res(6) OR l1res(4) OR l1res(2) OR l1res(0);
  lding1pos(1) <= l1res(21) OR l1res(20) OR l1res(17) OR l1res(16) OR
       l1res(13) OR l1res(12) OR l1res (9) OR l1res(8) OR
       l1res(5) OR l1res(4) OR l1res (1) OR l1res (0);
  lding1pos(2) <= l1res(19) OR l1res(18) OR l1res(17) OR l1res(16) OR
       l1res(11) OR l1res(10) OR l1res (9) OR l1res(8) OR
       l1res(3) OR l1res(2) OR l1res (1) OR l1res (0);
  lding1pos(3) <= l1res(15) OR l1res(14) OR l1res(13) OR l1res(12) OR
       l1res(11) OR l1res(10) OR l1res (9) OR l1res(8);
  lding1pos(4) <= l1res(7) OR l1res(6) OR l1res(5) OR l1res(4) OR
       l1res(3) OR l1res(2) OR l1res(1) OR l1res(0);
  -- generate exponent selections
  exp_val_p1 <= lgrexp XOR p1carry;
  p1carry(0) <= '1';
  p1carry(7 downto 1) <= lgrexp(6 downto 0) AND p1carry(6 downto 0);
  expp1all1 <= '1' when (exp_val_p1 = oneexp) else '0';
  adjexp <= '0' & lgrexp;
  resshf <= NOT("0000" & lding1pos);
  exp_val_adj <= adjexp XOR resshf XOR adjexpc;
  adjexpc(0) <= '1';
  adjexpc(8 downto 1) <= (adjexp(7 downto 0) AND resshf(7 downto 0)) OR
           (adjexp(7 downto 0) AND adjexpc(7 downto 0)) OR
           (resshf(7 downto 0) AND adjexpc(7 downto 0));
  underflow <= exp_val_adj(8);
  adjexp0 <= '1' when (exp_val_adj = "000000000") else '0';
  lgrall0 <= expa0 AND expb0;

  -- exponent selection signals that select result exponent
  exp_sel_p1 <= dec2;
  exp_sel_lgr <= (not dec2) and dec1;
  exp_sel_adj <= (not dec2) and (not dec1) and (not fract0) and 
                                            (not underflow);
  exp_sel_0 <= ((not dec2) AND (not dec1) and fract0) OR
                ((not dec2) AND (not dec1) and (not fract0) and underflow);
  -- select the proper output exponent
  exp_norm <= lgrexp      when ((expa1='1')AND(expb1='1')) else
              exp_val_p1  when (exp_sel_p1 = '1') else
              lgrexp      when (exp_sel_lgr = '1') else
              exp_val_adj(7 downto 0) when (exp_sel_adj = '1') else
              zeroexp;
  -- generate mantissa selection signals
  man_sel_ls <= (not dec2) and (not dec1) and (not (underflow OR adjexp0));
  man_sel_ls_rs <= (not dec2) and (not dec1) and (underflow or adjexp0);
  man_sel_val <= (not dec2) and dec1 and (not lgrall0);
  man_sel_0 <= dec2 and expp1all1;
  -- generate the mantissa selections
  --    do the left shift
  ls_dist <= lgrexp(4 downto 0) when (underflow = '1') else lding1pos;
  man_lshf1 <= addres(23 downto 0) & '0' when (ls_dist(0) = '1')
                       else addres;
  man_lshf2 <= man_lshf1(22 downto 0) & "00" when (ls_dist(1) = '1')
                       else man_lshf1;
  man_lshf3 <= man_lshf2(20 downto 0) & "0000" when (ls_dist(2) = '1')
                       else man_lshf2;
  man_lshf4 <= man_lshf3(16 downto 0) & "00000000" when (ls_dist(3) = '1')
                       else man_lshf3;
  man_lshf5 <= man_lshf4(8 downto 0) & "0000000000000000"
                     when (ls_dist(4) = '1') else man_lshf4;
  man_ls_rs <= '0' & man_lshf5(24 downto 1);
  man_rs1 <= '0' & addres(24 downto 1);
  -- now choose the correct mantissa
  man_norm <= '0' & nan_man when (((expa1 AND expb1 AND mana0 AND manb0)='1')
                                      AND((signa xor signb)='1')) else
              man_lshf5    when (man_sel_ls = '1') else
              man_ls_rs    when (man_sel_ls_rs = '1') else
              addres       when (man_sel_val = '1') else
              zero_24      when (man_sel_0 = '1') else
              man_rs1;

  Cint <= to_StdLogicVector(signr & exp_norm & man_norm (22 downto 0));
  

  drive_out : PROCESS (drive)
  BEGIN
    IF drive = '0'
      THEN  C <= Cint;
      ELSE  C <= HighZ;
    END IF;
  END PROCESS drive_out;

END data_flow;

