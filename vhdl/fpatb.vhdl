library ieee;
package float_pkg_cust is new IEEE.float_generic_pkg
  generic map (
    float_exponent_width => 8,    -- float32'high
    float_fraction_width => 23,   -- -float32'low
    float_round_style    => IEEE.fixed_float_types.round_nearest,  -- round nearest algorithm
    float_denormalize    => true,  -- Use IEEE extended floating
    float_check_error    => true,  -- Turn on NAN and overflow processing
    float_guard_bits     => 3,     -- number of guard bits
    no_warning           => false, -- show warnings
    fixed_pkg            => IEEE.fixed_pkg
);

library ieee;
use ieee.std_logic_1164.all;
use WORK.fpa_support.all;
use STD.TEXTIO.all;
use work.float_pkg_cust.all;

entity test_bench4 is
end test_bench4;


architecture my_test of test_bench4 is
  signal A,B,C,RES : std_logic_vector (31 downto 0);
  signal latch,drive : std_ulogic;
  signal aid_sig,bid_sig,res_id_sig : string (1 to 6);
  signal err_sig : bit;

COMPONENT fpa 
  PORT (A,B : IN std_logic_vector (31 downto 0);
        latch, drive: IN std_ulogic;
        C : OUT std_logic_vector (31 downto 0));
END COMPONENT;
FOR ALL : fpa USE  ENTITY WORK.fpa(data_flow);

BEGIN

a1 : fpa PORT MAP(A,B,latch,drive,C);


  applytest : PROCESS
    variable cur_line : LINE;
    file test_data : TEXT is IN "VP1/testvectors";
    variable a_test_val, b_test_val : bit_vector (31 downto 0);
    variable result_val : bit_vector (31 downto 0);
    variable std_result_val : std_logic_vector (31 downto 0);
    variable aid,bid,resid : string (1 to 6);
    variable float_result : float32;
    variable std_result : std_logic_vector (31 downto 0);
    
  Begin
    A <= HIGHZ; B <= HIGHZ; C <= HIGHZ;
    latch <= '1'; drive <= '1';
    WHILE (NOT ENDFILE(test_data)) LOOP
      --get next input test vector and expected result
      readline(test_data,cur_line);
      read(cur_line,aid); read(cur_line,a_test_val);
      read(cur_line,bid); read(cur_line,b_test_val);
      readline(test_data,cur_line);
      read(cur_line,resid);read(cur_line,result_val);
      std_result_val := To_StdLogicVector(result_val);
      RES <= std_result_val;
      -- run through bus cycle to send data to unit
      aid_sig <= "======"; bid_sig <= "======";
      wait for 10 ns;
      -- drive signals on bus
      aid_sig <= aid; bid_sig <= bid; res_id_sig <= resid;
      A <= To_StdLogicVector(a_test_val); 
      B <= To_StdLogicVector(b_test_val);
      wait for 20 ns;
      latch <= '0';
      wait for 50 ns;
      latch <= '1';
      wait for 10 ns;
      A <= HIGHZ; B <= HIGHZ;
      wait for 40 ns;
      drive <= '0';
      float_result := to_float(To_StdLogicVector(a_test_val)) + to_float(To_StdLogicVector(b_test_val));
      std_result := to_slv(float_result);
      wait for 40 ns;
      ASSERT (C = std_result)
	REPORT "result does not agree with expected flt result"
	SEVERITY WARNING;
      wait for 10 ns;
      drive <= '1';
      wait for 20 ns;
    END LOOP;
    wait for 20 ns;
    wait;
  END PROCESS;

END my_test;

