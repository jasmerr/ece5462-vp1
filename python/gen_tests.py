#!/usr/bin/env python
# -*- coding: utf-8 -*-
__author__ = "Adam Creech (creech.46@osu.edu)"
__version__ = "0.1.0"

import random
import ctypes

SIGN_LEN = 1
MANT_LEN = 23
EXP_LEN = 8
COMBINATIONS_PER_TEST = 1

def float_to_bin(float_val):
    """Converts a floating point number to its 32 bit IEEE-754 floating
       point binary representation.
    """

    bin_val = bin(ctypes.c_uint.from_buffer(float_val).value)[2:]
    bin_val = '0' * (32-len(bin_val)) + bin_val
    return bin_val

def random_mant():
    """Generates a random 23 bit mantissa
    """
    return ''.join([str(random.randint(0, 1)) for i in range(MANT_LEN)])

def random_exp():
    """Generates a random 8 bit exponent
    """
    return ''.join([str(random.randint(0, 1)) for i in range(EXP_LEN)])

def random_sign():
    """Generates a random 1 bit sign
    """
    return ''.join([str(random.randint(0, 1)) for i in range(SIGN_LEN)])

def random_nan():
    """Generates a random ieee 754 floating point number of type NaN
    :returns : string in format "...NaN X11111111XXXXXXXXXXXXXXXXXXXXXXX"
    """
    ret_mant = random_mant()
    while ret_mant == '0' * MANT_LEN:
        ret_mant = rand_mant()
    ret_sign = random_sign()

    return '...NaN ' + ret_sign + ('1' * EXP_LEN) + ret_mant

def pos_inf():
    """Generates a ieee 754 floating point number of type Positive Infinity
    :returns : string in format ".+INIF 01111111100000000000000000000000"
    """

    return '.+INIF 0' + ('1' * EXP_LEN) + ('0' * MANT_LEN)

def neg_inf():
    """Generates a ieee 754 floating point number of type Negative Infinity
    :returns : string in format ".-INIF 11111111100000000000000000000000"
    """

    return '.-INIF 1' + ('1' * EXP_LEN) + ('0' * MANT_LEN)

def pos_zero():
    """Generates a ieee 754 floating point number of type Positive Zero
    :returns : string in format "....+0 00000000000000000000000000000000"
    """

    return '....+0 0' + ('0' * EXP_LEN) + ('0' * MANT_LEN)

def neg_zero():
    """Generates a ieee 754 floating point number of type Negative Zero
    :returns : string in format "....-0 10000000000000000000000000000000"
    """

    return '....-0 1' + ('0' * EXP_LEN) + ('0' * MANT_LEN)

def random_pos_denorm():
    """Generates a random ieee 754 floating point number of type Positive Denormalized
    :returns : string in format "+DNORM 000000000XXXXXXXXXXXXXXXXXXXXXXX"
    """
    ret_mant = random_mant()
    while ret_mant == '0' * MANT_LEN:
        ret_mant = rand_mant()

    return '+DNORM 0' + ('0' * EXP_LEN) + ret_mant

def random_neg_denorm():
    """Generates a random ieee 754 floating point number of type Negative Denormalized
    :returns : string in format "-DNORM 100000000XXXXXXXXXXXXXXXXXXXXXXX"
    """
    ret_mant = random_mant()
    while ret_mant == '0' * MANT_LEN:
        ret_mant = rand_mant()

    return '-DNORM 1' + ('0' * EXP_LEN) + ret_mant

def random_pos_norm():
    """Generates a random ieee 754 floating point number of type Positive Normalized
    :returns : string in format ".+NORM 0XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    """
    ret_mant = random_mant()
    ret_exp = random_exp()
    while (ret_exp == '0' * EXP_LEN) or (ret_exp == '1' * EXP_LEN):
        ret_exp = random_exp()

    return '.+NORM 0' + ret_exp + ret_mant

def random_neg_norm():
    """Generates a random ieee 754 floating point number of type Negative Normalized
    :returns : string in format ".-NORM 1XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX"
    """
    ret_mant = random_mant()
    ret_exp = random_exp()
    while (ret_exp == '0' * EXP_LEN) or (ret_exp == '1' * EXP_LEN):
        ret_exp = random_exp()

    return '.-NORM 1' + ret_exp + ret_mant

def main():
    float_functions = [random_nan, pos_inf, neg_inf, pos_zero, neg_zero, random_pos_denorm,
                       random_neg_denorm, random_pos_norm, random_neg_norm]

    with open("../vhdl/randvectors", 'w') as f_out:
        print("Writing the following lines to ../vhdl/randvectors")
        # Generate tests T0-T96
        for input_A in float_functions:
            for input_B in float_functions:
                for i in range(COMBINATIONS_PER_TEST):
                    out_line = "{}{}".format(input_A(), input_B())
                    f_out.write(out_line)
                    print(out_line)

        

    


if __name__ == '__main__':
    
    try:
        main()
    except KeyboardInterrupt:
        raise SystemExit

