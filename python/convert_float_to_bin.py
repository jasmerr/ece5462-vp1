import ctypes

def float_to_bin(float_val):
    bin_val = bin(ctypes.c_uint.from_buffer(float_val).value)[2:]
    bin_val = '0' * (32-len(bin_val)) + bin_val
    return bin_val
