''' 
This file has one function which generates all 
testvector lines for the CXY corner input test
cases.
'''

corners = [
    'MAX+DN 00000000111111111111111111111111',
    'MIN+DN 00000000000000000000000000000001',
    'MAX-DN 10000000000000000000000000000001',
    'MIN-DN 10000000111111111111111111111111',
    '.MAX+N 01111110111111111111111111111111',
    '.MIN+N 00000001000000000000000000000000',
    '.MAX-N 11111110111111111111111111111111',
    '.MIN-N 10000001000000000000000000000000'
]

res_line = '...N/A XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX'

def gen_corner_input_lines():
    retStr = ''
    for c1 in corners:
        for c2 in corners:
            retStr += c1 + c2 + '\n' + res_line + '\n'
    
    return retStr[0:-1]